import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {

  menuItems = [{ name: 'Calculate', path: 'calculator-form' }, { name: 'History', path: 'history-list' }]

  constructor() { }

  ngOnInit(): void {
  }

}
