import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'calculator-form',
    pathMatch: 'full'
  },
  {
    path: 'calculator-form',
    loadChildren: () => import('./views/calculator-form/calculator-form.module').then(m => m.CalculatorFormModule)
  },
  {
    path: 'history-list',
    loadChildren: () => import('./views/history-list/history-list.module').then(m => m.HistoryListModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
