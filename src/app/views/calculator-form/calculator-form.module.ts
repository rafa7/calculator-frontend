import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CalculatorFormRoutingModule } from './calculator-form-routing.module';
import { CalculatorFormComponent } from './calculator-form.component';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    CalculatorFormComponent
  ],
  imports: [
    CommonModule,
    CalculatorFormRoutingModule,
    FormsModule
  ]
})
export class CalculatorFormModule { }
