import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/service/api.service';

@Component({
  selector: 'app-calculator-form',
  templateUrl: './calculator-form.component.html',
  styleUrls: ['./calculator-form.component.scss']
})
export class CalculatorFormComponent implements OnInit {

  fields: Array<any> = new Array();
  result: any;
  queryParams = {
    filters: Array<any>()
  }
  msgError = '';

  constructor(private apiService: ApiService) { }

  ngOnInit(): void {
    this.initializeFields();
  }

  add() {
    this.fields.push({value: null});
  }

  initializeFields() {
    this.fields = [ {value: null}, {value: null} ]
  }

  async calc() {
    this.msgError = '';
    this.queryParams.filters = this.fields;
    localStorage.setItem('query', JSON.stringify(this.queryParams.filters));
    try {
      const res: any = await this.apiService.calc(this.queryParams).toPromise();
      this.result = res.data;
    } catch (error) {
      this.result = null;
      this.msgError = error.error.error;
      console.log(error)
    }
    this.initializeFields();
  }

}
