import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/service/api.service';
import { camelizeKeys } from 'humps';

@Component({
  selector: 'app-history-list',
  templateUrl: './history-list.component.html',
  styleUrls: ['./history-list.component.scss']
})
export class HistoryListComponent implements OnInit {

  searchParams = {
    page: 1,
    perPage: 10
  };
  meta: any;
  historiesList = Array<any>();



  constructor(private apiService: ApiService) { }

  ngOnInit(): void {
    this.getHistories();
  }

  async getHistories() {
   try {
    const res: any = await this.apiService.getHistories(this.searchParams).toPromise();
    this.historiesList = camelizeKeys(res.data);
    this.meta = camelizeKeys(res.meta);
   } catch (error) {
     console.log(error);
   }
  }

  onPrev(): void {
    this.searchParams.page--;
    this.getHistories();
  }

  onNext(): void {
    this.searchParams.page++;
    this.getHistories();
  }

  onFirst(): void {
    this.searchParams.page = 1;
    this.getHistories();
  }

  onLast(): void {
    this.searchParams.page =  this.meta.totalPages;
    this.getHistories();
  }

  onPerPage(perPage: number): void {
    this.searchParams.page = 1;
    this.searchParams.perPage = perPage;
    this.getHistories();
  }

}
