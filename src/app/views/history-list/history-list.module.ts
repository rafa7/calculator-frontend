import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HistoryListRoutingModule } from './history-list-routing.module';
import { HistoryListComponent } from './history-list.component';
import { PaginationComponent } from 'src/app/components/pagination/pagination.component';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    HistoryListComponent,
    PaginationComponent,

  ],
  imports: [
    CommonModule,
    HistoryListRoutingModule,
    FormsModule
  ]
})
export class HistoryListModule { }
