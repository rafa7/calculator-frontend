import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { decamelizeKeys } from 'humps';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  apiUrl = environment.apiUrl;

  constructor(private http: HttpClient) { }

  getHistories(searchParameters: object = {}) {
    const params: any = decamelizeKeys(searchParameters);

    return this.http
      .get(`${this.apiUrl}/histories`, { params });
  }

  calc(parameters: object = {}) {
    const params: any = decamelizeKeys(parameters);

    return this.http
      .post(`${this.apiUrl}/calc`, params);
  }
}
