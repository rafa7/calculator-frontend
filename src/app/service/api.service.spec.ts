import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { ApiService } from './api.service';
import { environment } from 'src/environments/environment';

describe('ApiService', () => {
  let service: ApiService;
  let httpTestingController: HttpTestingController;
  let apiUrl = environment.apiUrl;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule]
    });
    service = TestBed.inject(ApiService);
    httpTestingController = TestBed.inject(HttpTestingController);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should expect a GET /histories', () => {
  
    // Make an HTTP GET request
    service.getHistories().subscribe();
  
    const req = httpTestingController.expectOne(`${apiUrl}/histories`);
  
    expect(req.request.method).toEqual('GET');
  
    httpTestingController.verify();
  });

  it('should expect a POST /calc', () => {
  
    // Make an HTTP POST request
    service.calc().subscribe();
  
    const req = httpTestingController.expectOne(`${apiUrl}/calc`);
  
    expect(req.request.method).toEqual('POST');

  
    httpTestingController.verify();
  });

});
